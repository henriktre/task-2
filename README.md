# Handin 2 - Henrik Trehjørningen
## Project setup
### Compile all java files
```
javac -d out src/java/*.java src/java/manipulation/*.java src/java/utilities/*.java src/java/logger/*.java
```
### change directory to the out folder
```
cd out
```
### bundle all class files into a .jar executable
```
jar cfe Program.jar Program *.class manipulation/*.class utilities/*.class logger/*.class  
```

### Execute the jar file
```
java -jar Program.jar 
```



### Screen grab of the process.

![alt text](screen.png "Screen grab")