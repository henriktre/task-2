package manipulation;

import utilities.*; //my utility functions

import java.io.BufferedReader; //used to count lines of the text file
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files; //used for the readAllLines
import java.util.*;

import logger.Logger; //my logger

public class Manipulation {
    public static String resources = "resources";
    public static File textFile = null;


    public static File getTextFile() {  //returns the first file that has .txt extension
        File[] files = new File(resources).listFiles();
        for (File file : files) {
            String name = file.getName();
            if (name.substring(name.indexOf(".") + 1).equals("txt")) {
                return file; 
            }
        }
        return null;
    }

    public static int nrOfLines() { //uses BufferedReader to read one line at the time and returns the number of lines
        long startTime = System.currentTimeMillis();
        try {
            BufferedReader r = new BufferedReader(new FileReader(textFile.getAbsolutePath()));
            int lines = 0;
            while ((r.readLine()) != null){
                lines++;
            }
            r.close();
            Logger.logAction("The number of lines is " + lines, startTime);
            return lines;
        }
        catch(IOException e){
            System.out.println(e);
        }
        catch(Exception e){
            System.out.println(e);
        }
        
        return 0;
    }

    // if singleWord is true then it will return 1 after the first occurance
    // of the input word, if it is false then it will return the total number of occurrences.
    public static int countWords(String word, boolean singleWord) {
        long startTime = System.currentTimeMillis();

        ArrayList<String> words = new ArrayList<String>();
        List<String> lines;
        int occurrences = 0;
        word = word.trim().toLowerCase();
        try {
            lines = Files.readAllLines(textFile.toPath()); //A list of strings containing all the lines in the file
            lines.forEach(line -> {
                Collections.addAll(words, line.split(" ")); // Splitting the line into single words 
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


        for(String e : words) { //iterating through all the words to check if they match with the input word and incrementing occurrences
            if(e.toLowerCase().equals(word)) { // unless singleWord is true then it returns after first match
                if(singleWord) {
                    Logger.logAction("The word \"" + e + "\" exists", startTime);
                    return 1;
                }
                occurrences++;
            }
        }


        // words.forEach(e -> {
        //     if(e.equals(word)) {
        //         occurrences++; <---- illegal, why?
        //     }
        // });

        Logger.logAction("The word \"" + word + "\" was found " + occurrences + " times ", startTime);
        return occurrences;
    }

    public static void listAllFiles() { // just lists all files in resources folder

        try {
            String[] fileNames = new File(resources).list();
            for (String fileName : fileNames) {
                System.out.println(fileName);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void listAllFilesWithExtension() { // same as above but compares a user input extension with the extensions of the files.
        System.out.println("Input a file extension to find: ");
        String extension = Utils.userStringInput();
        try {
            String[] fileNames = new File(resources).list();
            for (String fileName : fileNames) {

                if (fileName.substring(fileName.indexOf(".") + 1).equals(extension)) {
                    System.out.println(fileName);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void manipulate() { //Lists details of the text file and asks user if he/she wants to count a words occurance.
        String name = textFile.getName();
        System.out.println("Name of the file: " + name);
        long startTime = System.currentTimeMillis();
        long length = textFile.length();
        Logger.logAction("The size of " + name + " is " + length/ 1024, startTime);
        System.out.println("Size of " + name + ": " + length / 1024 + " kb.");

        System.out.println("Lines in " + name + ": " + nrOfLines());
        char choice = 'y';
        Scanner s = new Scanner(System.in);
        System.out.println("Do you want to search for a word in " + name + "? (y)es / (n)o");
        choice = s.next().toLowerCase().charAt(0);
        if(Character.compare(choice, 'y') == 0){
            System.out.println("Enter the word you want to search for in " + name + ": ");
            String word = Utils.userStringInput();
            if(countWords(word, true) == 1){
                System.out.println("The word \"" + word + "\" exists in " + name);
            } else {
                System.out.println("The word \"" + word + "\" does not exist in " + name);
            }
            int occurance = countWords(word, false);
            if(occurance > 0) {
                System.out.println("The word \"" + word + "\" occurs " + occurance + " times in " + name);
            }
        }
    }

    public static void menu() {
        textFile = getTextFile();
        String[] prompts = new String[] { "Choose a number (1-3) or press e to return to main menu",
                "1: List all file names in a directory", "2: List all file names with a given extension",
                "3: Manipulate a given text file", "e: Exit" };

        char choice = 'y';
        Scanner s = new Scanner(System.in);

        while (choice != 'e') {
            Utils.menu(prompts);
            choice = s.next().toLowerCase().charAt(0);
            switch (choice) {
                case '1':
                    listAllFiles();
                    break;
                case '2':
                    listAllFilesWithExtension();
                    break;
                case '3':
                    manipulate();
                    break;
                case 'e':
                    System.out.println("Bye bye");
                    break;
                default:
                    System.out.println("Invalid choice: " + choice);
                    break;
            }
        }
    }
}
