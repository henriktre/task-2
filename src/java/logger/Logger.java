package logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

public class Logger {
    public static File log;

    public static void createFile(String name) { //creating a log file if none exists
        log = new File(name);
        try {
            if (log.createNewFile()) {
                System.out.println("The file " + name + " was created successfully");
            } else {
                System.out.println("The file " + name + " already exists");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void logAction(String action, long startTime) { //logs an action with a message + a time that was before the execution
        PrintWriter out = null;
        try {
            FileWriter fileWriter = new FileWriter(log, true); //true to do append mode
            out = new PrintWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Date date = new Date(); //logs Timestamp + message + exec time 
        out.println(new Timestamp(date.getTime()) + ": " + action + " and the funciton took " + (System.currentTimeMillis() - startTime) + " milliseconds to execute.");
        out.close();
        out.flush();
    }
}
