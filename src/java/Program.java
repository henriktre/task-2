import manipulation.*;
import logger.*;

class Program {
    public static void main(final String[] args) {
        long start = System.currentTimeMillis();
        Logger.createFile("log.txt");
        Logger.logAction("Logger Createfile ran", start);
        Manipulation.menu(); //main menu
    }
}