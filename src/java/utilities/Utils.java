package utilities;

import java.util.Scanner;

import logger.Logger;

public class Utils {
    public static void clearScreen() {  //thanks stack overflow
        long startTime = System.currentTimeMillis();
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
        Logger.logAction("Utils clearScreen ran", startTime);
    }  

    public static String userStringInput() {
        try {
            Scanner s = new Scanner(System.in);
            String input = s.nextLine();
            return input.trim().toLowerCase().split(" ")[0];
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        
    }
    public static void menu (String[] prompts) {
        //clearScreen();
        String line = "~ . ".repeat(17);
        System.out.println(line);
        for( String tmp : prompts) {
            System.out.println("| " + tmp);
        }   
        System.out.println(line);
    }

}